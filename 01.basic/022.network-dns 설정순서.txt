﻿* 도메인과 서버를 연결시키는 순서 *
참고: https://server-99.com/dns-record/

1. 도메인 구입: ex) example.com
2. 서브 도메인 결정: ex) dev.example.com
3. DNS에 연결할 AWS의 서버 주소:
	ex)
	Elastic beanstalk url: dev.ap-northeast-1.elasticbeanstalk.com
	Elastic IPs: 52.15.12.153 (엘라스틱 ip가 있는 경우)

4. 서브 도메인과 서버를 연결
	1) CNAME 레코드로 연결 할 경우:
		- 호스트명	: dev.example.com
		- Type		: CNAME 레코드
		- TTL		: 3600
		- VALUE		: dev.ap-northeast-1.elasticbeanstalk.com

	2) A레코드로 연결 할 경우: (고정ip 인 경우만 사용가능. 유동ip인 경우, ip가 변동되면 변동될때마다 재설정 해줘야함)
		- 호스트명	: dev.example.com
		- Type		: A 레코드
		- TTL		: 3600
		- Value		: 52.15.12.153

5. 호스트명 접속 테스트