﻿var app = angular.module("NandemoApp", []);
app.controller("NandemoCtrl", function ($scope, $timeout, $interval) {
    /*******************************************************************************************
    설정
    *******************************************************************************************/
    // 선언
    $scope.Main = {
        CurrentPage: "InputPage",
        Init: function () {
        },
        Dispose: function () {
            $scope.Main.CurrentPage = "InputPage";
        }
    };

    $scope.Input = {
        UserInputData: "",
        DataSet: [],
        Init: function () {
        },
        Dispose: function () {
            $scope.Input.UserInputData = "";
            $scope.Input.DataSet = [];
        },
        InputData: function () {

            // DataSet 초기화
            $scope.Input.DataSet = [];

            // 사용자의 입력값을 읽어, DataSet에 넣는다.
            let lines = $scope.Input.UserInputData.split(/\r?\n/).filter(function (x) {
                return x != "";
            });

            for (let lineIdx = 0; lineIdx < lines.length; lineIdx++) {

                let currentline = lines[lineIdx].split(",");

                // 3.1. 데이터 유효성 검사
                // A 또는 B가 없는 경우
                if (currentline.length < 2) {
                    alert("데이터가 올바르지 않습니다.");
                    return;
                }

                // A 또는 B 값이 비어있는 경우 
                if (currentline[0] == "" || currentline[1] == "") {
                    alert("데이터가 올바르지 않습니다.");
                    return;
                }

                // 3.2. dataSet에 data를 넣는다.
                let data = {};
                data["Idx"] = lineIdx;
                data["A"] = currentline[0];
                data["B"] = currentline[1];
                data["Hint"] = currentline.length > 2 ? currentline[2] : "";

                $scope.Input.DataSet.push(data);
            }
        },
        GotoPlayPage: function () {
            $scope.Main.CurrentPage = "PlayPage";
            $scope.Puzzle.Init();
        }
    };

    $scope.Puzzle = {
        DataSet: [],
        System: {
            Parameters: {
                Speed: 1000
            },
            Status: {
                GameProgress: "Wait" // Wait: Players 등록을 기다리고 있음, Ready:게임시작 준비완료, Playing:게임 중, Finished:게임 종료
            },
            Interval: undefined,
            Action: function () {

                // Game Over인지 여부를 결정한다.
                // 1) 모든 Brick의 IsSolved == true 인 경우
                // 2) 마지막 남은 두장의 Brick을, Player 둘이 하나씩 선택했을 경우
                let solvedBricksCount = 0;
                for (let i = 0; i < $scope.Puzzle.DataSet.length; i++) {
                    if ($scope.Puzzle.DataSet[i].IsSolved || $scope.Puzzle.DataSet[i].IsSolved == false && $scope.Puzzle.DataSet[i].Owner != null && $scope.Puzzle.DataSet[i].IsUnreachable == false)
                        solvedBricksCount = solvedBricksCount + 1;
                };

                if (solvedBricksCount == $scope.Puzzle.DataSet.length) {
                    $scope.Puzzle.System.Status.GameProgress = "Finished";

                    // 1. System의 Action을 종료한다.
                    $interval.cancel($scope.Puzzle.System.Interval);
                    $scope.Puzzle.System.Interval = undefined;

                    // AI 플레이어를 종료한다. (AI와의 대전이어서 들어간 부분)
                    $interval.cancel($scope.Puzzle.Players[1].Interval);
                    $scope.Puzzle.Players[1].Interval = undefined;

                    // 2. 게임 결과를 만들고, 보여준다.
                    let gameResult = "";
                    if ($scope.Puzzle.Players[0].Status.Score > $scope.Puzzle.Players[1].Status.Score)
                        gameResult = "You win !";
                    else if ($scope.Puzzle.Players[0].Status.Score == $scope.Puzzle.Players[1].Status.Score)
                        gameResult = "Draw !";
                    else
                        gameResult = "You lose !";

                    alert(gameResult);

                    // 모든 플레이어가 준비를 완료했다고 가정한다. (AI와의 대전이어서 들어간 부분)
                    $scope.Puzzle.PlayersReady();
                }
            }
        },
        Players: [{
            // User 1
            Parameters: {
                Name: "You",
                BrickColorPressed: "LightSkyBlue",
                BrickColorSolved: "DeepSkyBlue",
                Level: 1
            },
            Status: {
                Score: 0
            }
        },
        {
            // User 2 (AI)
            Parameters: {
                Name: "AI",
                BrickColorPressed: "MistyRose",
                BrickColorSolved: "Tomato",
                Level: 1,
                Brain: [],
                BrainSize: 1,
                SetFound: [],
                Speed: 2000
            },
            Status: {
                Score: 0,
            },
            Action: {
                Routine: function () {

                    // SetFound가 있는 경우
                    if ($scope.Puzzle.Players[1].Parameters.SetFound.length > 0) {

                        let brickToPush = $scope.Puzzle.Players[1].Parameters.SetFound[0];
                        $scope.Puzzle._BrickPress(brickToPush.BrickIdx, 1);

                        // Unreachable 상태인 경우, 다시 시도 한다.
                        if ($scope.Puzzle.DataSet[brickToPush.BrickIdx].IsUnreachable)
                            return;

                        // 매칭이 완료된 벽돌의 경우, AI의 SetFound를 초기화한다.
                        if ($scope.Puzzle.DataSet[brickToPush.BrickIdx].IsSolved) {
                            $scope.Puzzle.Players[1].Parameters.SetFound = [];
                            return;
                        }

                        // 안 눌린 경우, 다시 시도 한다.
                        if ($scope.Puzzle.DataSet[brickToPush.BrickIdx].Owner != 1)
                            return;

                        // 눌렸고, 매칭이 완료되지 않은 경우, SetFound에서 누른 Brick을 삭제한다.
                        $scope.Puzzle.Players[1].Parameters.SetFound.shift();
                        return;
                    }

                    // SetFound가 없는 경우
                    // 안 눌리고 메모리에 없는 Brick들을 찾는다.
                    let notPushedBrickIdxs = [];
                    for (let i = 0; i < $scope.Puzzle.DataSet.length; i++) {
                        if ($scope.Puzzle.DataSet[i].Owner == null) {
                            let memoryIndex = $scope.Puzzle.Players[1].Parameters.Brain.findIndex(function (m) { return m.BrickIdx == i });
                            if (memoryIndex == -1)
                                notPushedBrickIdxs.push(i);
                        }
                    };

                    // 그 중에서 하나를 선택한다.
                    if (notPushedBrickIdxs.length > 0) {
                        notPushedBrickIdxs.sort(function (a, b) { return 0.5 - Math.random() });

                        let brickToPush = $scope.Puzzle.DataSet[notPushedBrickIdxs[0]];
                        brickToPush.BrickIdx = notPushedBrickIdxs[0];

                        // 선택한 Brick을 누른다.
                        $scope.Puzzle._BrickPress(brickToPush.BrickIdx, 1);

                        // Brick이 이미 눌려져 있던 경우, 아무것도 안한다.
                        if ($scope.Puzzle.DataSet[brickToPush.BrickIdx].Owner != 1)
                            return;

                        // 눌러서 매칭된 경우, 메모리에서 성공한 Brick의 짝이 있는 경우 지운다.
                        if ($scope.Puzzle.DataSet[brickToPush.BrickIdx].IsSolved) {
                            let indexFound = -1;
                            for (let i = 0; i < $scope.Puzzle.Players[1].Parameters.Brain.length; i++) {
                                if ($scope.Puzzle.Players[1].Parameters.Brain[i].Id == brickToPush.Id) {
                                    indexFound = i;
                                    break;
                                }
                            }
                            if (indexFound >= 0)
                                $scope.Puzzle.Players[1].Parameters.Brain.splice(indexFound, 1);

                            return;
                        }

                        // 눌러서 매칭되지 않은 경우
                        // Brain에 기억이 있는 경우
                        if ($scope.Puzzle.Players[1].Parameters.Brain.length > 0) {
                            let indexFound = -1;

                            for (let i = 0; i < $scope.Puzzle.Players[1].Parameters.Brain.length; i++) {
                                // ID가 같은게 있는 경우.
                                if ($scope.Puzzle.Players[1].Parameters.Brain[i].Id == brickToPush.Id) {
                                    indexFound = i;
                                    break;
                                }
                            }

                            if (indexFound >= 0) {
                                // Brain에 매칭되는 Brick이 있는 경우, Brain에 있던 Brick과 선택한 Brick의 쌍을 만들어 SetFound로 옮긴다. 옮긴 Brick은 Brain에서 지운다.
                                $scope.Puzzle.Players[1].Parameters.SetFound.push($scope.Puzzle.Players[1].Parameters.Brain[indexFound], { Text: brickToPush.Text, Id: brickToPush.Id, BrickIdx: brickToPush.BrickIdx })
                                $scope.Puzzle.Players[1].Parameters.Brain.splice(indexFound, 1);
                            }
                            else {
                                // Brain에 매칭되는 Brick이 없는 경우, 선택한 Brick을 Brain에 넣는다. Brain이 이미 꽉 찬 경우는 아무것도 하지 않는다.
                                if ($scope.Puzzle.Players[1].Parameters.Brain.length == $scope.Puzzle.Players[1].Parameters.BrainSize) { }
                                else
                                    $scope.Puzzle.Players[1].Parameters.Brain.push({ Text: brickToPush.Text, Id: brickToPush.Id, BrickIdx: brickToPush.BrickIdx });
                            }
                        }
                        else {
                            // Brain에 기억이 없는 경우, Brain에 선택한 Brick을 넣는다. Brain이 이미 꽉 찬 경우는 아무것도 하지 않는다.
                            if ($scope.Puzzle.Players[1].Parameters.Brain.length == $scope.Puzzle.Players[1].Parameters.BrainSize) { }
                            else
                                $scope.Puzzle.Players[1].Parameters.Brain.push({ Text: brickToPush.Text, Id: brickToPush.Id, BrickIdx: brickToPush.BrickIdx });
                        }
                    };
                }
            },
            Interval: undefined        
        }],
        Init: function () {
            // 모든 플레이어가 준비를 완료했다고 가정한다. (AI와의 대전이어서 들어간 부분)
            $scope.Puzzle.PlayersReady();
        },
        Dispose: function () {
            $scope.Puzzle.DataSet = [];
            $scope.Puzzle.System.Parameters.Speed = 1000;
            $scope.Puzzle.System.Status.GameProgress = "Wait";
            $scope.Puzzle.System.Interval = undefined;
            
            for (let i = 0; i < $scope.Puzzle.Players.length; i++) {
                $scope.Puzzle.Players[i].Parameters.Name = "";
                $scope.Puzzle.Players[i].Parameters.BrickColorPressed = "";
                $scope.Puzzle.Players[i].Parameters.BrickColorSolved = "";
                $scope.Puzzle.Players[i].Parameters.Level = 1;
                $scope.Puzzle.Players[i].Status.Score = 0;
            }

            // User 플레이어를 Dispose한다. (AI와의 대전이어서 들어간 부분)
            $scope.Puzzle.Players[0].Parameters.Name = "You";
            $scope.Puzzle.Players[0].Parameters.BrickColorPressed = "LightSkyBlue";
            $scope.Puzzle.Players[0].Parameters.BrickColorSolved = "DeepSkyBlue";
            // AI 플레이어를 Dispose한다. (AI와의 대전이어서 들어간 부분)
            $scope.Puzzle.Players[1].Parameters.Name = "AI";
            $scope.Puzzle.Players[1].Parameters.BrickColorPressed = "MistyRose";
            $scope.Puzzle.Players[1].Parameters.BrickColorSolved = "Tomato";
            $scope.Puzzle.Players[1].Parameters.Brain = [];
            $scope.Puzzle.Players[1].Parameters.BrainSize = 1;
            $scope.Puzzle.Players[1].Parameters.SetFound = [];
            $scope.Puzzle.Players[1].Parameters.Speed = 2000;
            $scope.Puzzle.Players[1].Interval = undefined;
        },
        PlayersReady: function(){
            // 모든 플레이어가 준비를 완료 한 경우 (AI와의 대전이어서 들어간 부분)
            $scope.Puzzle.System.Status.GameProgress = "Ready";
        },
        PlayersLoad: function () {
            // 각 플레이어의 설정을 Load 한다.
            for (let i = 0; i < $scope.Puzzle.Players.length; i++) {
                $scope.Puzzle.Players[i].Status.Score = 0;
            }
        },
        GameLoad: function () {
            // 1. 게임을 Load 한다.
            // 1.1) 퍼즐을 $scope.Puzzle.DataSet에 Load한다.
            $scope.Input.DataSet.sort(function (a, b) { return 0.5 - Math.random() });

            let maxPuzzleLength = 8; // 퍼즐쌍의 최대 갯수를 제한한다.
            if ($scope.Input.DataSet.length < 8)
                maxPuzzleLength = $scope.Input.DataSet.length;

            $scope.Puzzle.DataSet = [];
            for (let i = 0; i < maxPuzzleLength; i++) {
                // A 셋트
                $scope.Puzzle.DataSet.push(
                    {
                        Id: $scope.Input.DataSet[i].Idx,
                        Text: $scope.Input.DataSet[i].A,
                        Hint: $scope.Input.DataSet[i].Hint,
                        DisplayText: "Press",
                        Owner: null,            // 벽돌의 소유자
                        IsUnreachable: false,   // 벽돌이 게임에서 제외된 상태인지 여부
                        IsSolved: false         // 벽돌의 상태 - false: 초기, true: 풀렸음
                    }
                );
                // B 셋트
                $scope.Puzzle.DataSet.push(
                    {
                        Id: $scope.Input.DataSet[i].Idx,
                        Text: $scope.Input.DataSet[i].B,
                        Hint: $scope.Input.DataSet[i].Hint,
                        DisplayText: "Press",
                        Owner: null,            // 벽돌의 소유자
                        IsUnreachable: false,   // 벽돌이 게임에서 제외된 상태인지 여부
                        IsSolved: false         // 벽돌의 상태 - false: 초기, true: 풀렸음
                    }
                );
            }

            // 퍼즐 데이터 순서 섞기
            $scope.Puzzle.DataSet.sort(function (a, b) { return 0.5 - Math.random() });

            // 2. 게임을 관리할 System AI를 Load 한다.
            $scope.Puzzle.System.Parameters.Speed = 1000;
        },
        GameStart: function () {
            // 1. 플레이어들을 Load 한다.
            $scope.Puzzle.PlayersLoad();

            // 2. 게임을 Load 한다.
            $scope.Puzzle.GameLoad();

            // 3. 게임을 시작한다.
            // 3.1) 게임 시작을 알린다.
            alert("Game Start !");

            // 3.2) 게임을 관리할 System AI를 시작한다.
            $scope.Puzzle.System.Status.GameProgress = "Playing";
            $scope.Puzzle.System.Interval = $interval($scope.Puzzle.System.Action, $scope.Puzzle.System.Parameters.Speed);

            // 3.3) AI 플레이어를 시작한다. (AI와의 대전이어서 들어간 부분)
            $scope.Puzzle.Players[1].Interval = $interval($scope.Puzzle.Players[1].Action.Routine, $scope.Puzzle.Players[1].Parameters.Speed);
        },
        _BrickPress: function (brickIdx, owner) {

            // 사용자가 지금 누른 벽돌
            let pushedBrick;

            pushedBrick = $scope.Puzzle.DataSet[brickIdx];

            // 1. 사용자가 지금 누른 벽돌이, 이미 눌려져 있는 경우, return
            if (pushedBrick.Owner != null)
                return;
              
            // 2. 사용자가 지금 누른 벽돌이, 눌려져 있지 않은 경우, 누름
            pushedBrick.DisplayText = pushedBrick.Text;
            pushedBrick.Owner = owner;

            // 사용자가 이전에 선택한 벽돌
            let selectedBrick;

	        for (let i = 0; i < $scope.Puzzle.DataSet.length; i++) {
		    if (i != brickIdx && $scope.Puzzle.DataSet[i].IsUnreachable == false && $scope.Puzzle.DataSet[i].IsSolved == false && $scope.Puzzle.DataSet[i].Owner == owner)
			    selectedBrick = $scope.Puzzle.DataSet[i];
	        }

            // 사용자가 이전에 선택한 벽돌이 있는 경우
            if (selectedBrick != undefined) {
                
                if (selectedBrick.Id == pushedBrick.Id) {

                    // 매칭 된 경우
                    selectedBrick.IsSolved = true;
                    pushedBrick.IsSolved = true;

                    // 점수설정
                    for (let i = 0; i < $scope.Puzzle.Players.length; i++) {
                        if (i == owner)
                            $scope.Puzzle.Players[i].Status.Score++;
                    }
                }
                else {

                    // 매칭 되지 않은 경우
                    selectedBrick.IsUnreachable = true;
                    pushedBrick.IsUnreachable = true;

                    $timeout(function () {
                        selectedBrick.DisplayText = "Press";
                        selectedBrick.Owner = null;
                        selectedBrick.IsUnreachable = false;

                        pushedBrick.DisplayText = "Press";
                        pushedBrick.Owner = null;
                        pushedBrick.IsUnreachable = false;
                    }, 750);
                }
            }            
        },
        _BrickColor: function (owner, isMatched) {
            let color = "white";

            if (owner == null) { }
            else if (isMatched)
                color = $scope.Puzzle.Players[owner].Parameters.BrickColorSolved;
            else
                color = $scope.Puzzle.Players[owner].Parameters.BrickColorPressed;

            return color;
        }
    };

    // 동작
    $scope.Main.Init();
});