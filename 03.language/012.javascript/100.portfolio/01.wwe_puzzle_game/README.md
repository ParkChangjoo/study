# Puzzle Game
WWE選手の写真と選手の名前をマッチングするウェーブパズルゲームです。  

- - -

### Source code
https://bitbucket.org/ParkChangjoo/parkchangjoo.bitbucket.io/src/master/
### Gameplay
以下のurlに接続しゲームをプレイする事が出来ます。  
https://parkchangjoo.bitbucket.io/PuzzleGame.html  
> Bitbucket static websiteの利用によって、content security policy reportの表示やゲーム速度低下が発生します。  
> Clone後Localでテスト可能です。実行ファイルはPuzzleGame.htmlです。
### Browser support
Chrome 85.0.4183.121
### Language
HTML5/CSS, Javascript
### Framework
AngularJs
### Author
Park Changjoo