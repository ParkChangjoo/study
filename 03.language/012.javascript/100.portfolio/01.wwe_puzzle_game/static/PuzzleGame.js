﻿var app = angular.module("PuzzleApp", []);
app.controller("PuzzleCtrl", function ($scope, $timeout, $interval) {
    /*******************************************************************************************
    WWEマッチングゲームプログラム
    *******************************************************************************************/
    // プログラム基本動作設定
    $scope.Main = {
        // 現在のページ
        CurrentPage: "StartPage",
        // 初期化
        Init: function () {
            $scope.Database.Init();
        },
        // イントロページ1へ移動
        GotoTutorialPage1: function () {
            $scope.Main.CurrentPage = "TutorialPage1";
        },
        // イントロページ2へ移動
        GotoTutorialPage2: function () {
            $scope.Main.CurrentPage = "TutorialPage2";
        },
        // イントロページ3へ移動
        GotoTutorialPage3: function () {
            $scope.Main.CurrentPage = "TutorialPage3";
        },
        // イントロページ4へ移動
        GotoTutorialPage4: function () {
            $scope.Main.CurrentPage = "TutorialPage4";
        },
        // イントロページ5へ移動
        GotoTutorialPage5: function () {
            $scope.Main.CurrentPage = "TutorialPage5";
        },
        // イントロページ6へ移動
        GotoTutorialPage6: function () {
            $scope.Main.CurrentPage = "TutorialPage6";
        },
        // ゲームプレイページへ移動
        GotoPlayPage: function () {
            $scope.Main.CurrentPage = "PlayPage";
            $scope.Puzzle.Init();
            $scope.Puzzle.GameStart();
        }
    };

    // データベース
    $scope.Database = {
        Wrestler: [],
        Init: function () {
            // Wrestlerテーブル
            $scope.Database.Wrestler = [];

            // RawデータをWrestlerテーブルに入力
            let rawData = "img/asuka.jpg,img/asuka_txt.jpg;img/bookert.jpg,img/bookert_txt.jpg;img/hulk.jpg,img/hulk_txt.jpg;img/johncena.jpg,img/johncena_txt.jpg;img/kane.jpg,img/kane_txt.jpg;img/kurt.jpg,img/kurt_txt.jpg;img/rey.jpg,img/rey_txt.jpg;img/stone.jpg,img/stone_txt.jpg;img/tripleh.jpg,img/tripleh_txt.jpg;img/undertaker.jpg,img/undertaker_txt.jpg";
            let sets = rawData.split(";").filter(function (x) {
                return x != "";
            });

            for (let idx = 0; idx < sets.length; idx++) {
                let set = sets[idx].split(",");
                let data = {};
                data["Idx"] = idx;
                data["A"] = set[0];
                data["B"] = set[1];

                $scope.Database.Wrestler.push(data);
            }
        }
    };

    // マッチングゲーム動作設定
    $scope.Puzzle = {
        // パズルゲームのデータ
        DataSet: [],
        // ゲームの状態
        Status: {
            GameProgress: "Wait" // Wait: プレイアの登録を待っている状態, Ready:ゲームスタート準備完了状態, Playing:ゲーム中, Finished:ゲーム終了状態
        },
        // システムAI
        System: {
            // システムAIのConfig
            Parameters: {
                Speed: 1000
            },
            Interval: undefined,
            // システムAIの動作
            Action: function () {
                // ゲーム終了であるか判断する
                // * ゲーム終了の条件
                // 1) すべてのBrickのIsSolved == trueの場合
                // 2) 最後に残っている2つのBrickを, Player二人が1つづづ選択した場合
                let solvedBricksCount = 0;
                for (let i = 0; i < $scope.Puzzle.DataSet.length; i++) {
                    if ($scope.Puzzle.DataSet[i].IsSolved || $scope.Puzzle.DataSet[i].IsSolved == false && $scope.Puzzle.DataSet[i].Owner != null && $scope.Puzzle.DataSet[i].IsUnreachable == false)
                        solvedBricksCount = solvedBricksCount + 1;
                };

                if (solvedBricksCount == $scope.Puzzle.DataSet.length) {
                    $scope.Puzzle.Status.GameProgress = "Finished";

                    // 1. システムAIのアクションを終了
                    $interval.cancel($scope.Puzzle.System.Interval);
                    $scope.Puzzle.System.Interval = undefined;

                    // 2. CPUプレイアを終了
                    $interval.cancel($scope.Puzzle.Players[1].Interval);
                    $scope.Puzzle.Players[1].Interval = undefined;

                    // 3. ゲームの結果生成
                    let gameResult = "";
                    if ($scope.Puzzle.Players[0].Status.Score > $scope.Puzzle.Players[1].Status.Score)
                        gameResult = "You win !";
                    else if ($scope.Puzzle.Players[0].Status.Score == $scope.Puzzle.Players[1].Status.Score)
                        gameResult = "Draw !";
                    else
                        gameResult = "You lose !";

                    alert(gameResult);

                    // 4. ゲーム状態変更
                    $scope.Puzzle.Status.GameProgress = "Ready";
                }
            }
        },
        // プレイア
        Players: [{
            // User
            Parameters: {
                Name: "You",//名前
                BrickColorPressed: "gray",//押された時のBrickのborder color
                BrickColorSolved: "black"//マッチングされた時のBrickのborder color
            },
            Status: {
                Score: 0
            }
        },
        {
            // CPU
            Parameters: {
                Name: "CPU",
                BrickColorPressed: "red",
                BrickColorSolved: "crimson",
                Brain: [],//押したBrickを記憶する場所
                BrainSize: 1,//Brainの大きさ
                SetFound: [],//マッチングを発見した時、マッチングBrickを記憶する場所
                Speed: 1500//Brickを押す速度
            },
            Status: {
                Score: 0,
            },
            Action: {
                Routine: function () {

                    // SetFoundがEmptyではない場合
                    if ($scope.Puzzle.Players[1].Parameters.SetFound.length > 0) {

                        let brickToPush = $scope.Puzzle.Players[1].Parameters.SetFound[0];
                        $scope.Puzzle._BrickPress(brickToPush.BrickIdx, 1);

                        // 押せない状態の場合, もう一度try
                        if ($scope.Puzzle.DataSet[brickToPush.BrickIdx].IsUnreachable)
                            return;

                        // マッチング完了のBrickの場合, SetFoundを初期化する。
                        if ($scope.Puzzle.DataSet[brickToPush.BrickIdx].IsSolved) {
                            $scope.Puzzle.Players[1].Parameters.SetFound = [];
                            return;
                        }

                        // 他のユーザーが押して押せない場合、もう一度try
                        if ($scope.Puzzle.DataSet[brickToPush.BrickIdx].Owner != 1)
                            return;

                        // SetFoundから押したBrickを削除
                        $scope.Puzzle.Players[1].Parameters.SetFound.shift();
                        return;
                    }

                    // SetFoundが存在していない場合
                    // 押されていない、Brainにも存在していないBrickを探す
                    let notPushedBrickIdxs = [];
                    for (let i = 0; i < $scope.Puzzle.DataSet.length; i++) {
                        if ($scope.Puzzle.DataSet[i].Owner == null) {
                            let memoryIndex = $scope.Puzzle.Players[1].Parameters.Brain.findIndex(function (m) { return m.BrickIdx == i });
                            if (memoryIndex == -1)
                                notPushedBrickIdxs.push(i);
                        }
                    };

                    // その中で1つを選択
                    if (notPushedBrickIdxs.length > 0) {
                        notPushedBrickIdxs.sort(function (a, b) { return 0.5 - Math.random() });

                        let brickToPush = $scope.Puzzle.DataSet[notPushedBrickIdxs[0]];
                        brickToPush.BrickIdx = notPushedBrickIdxs[0];

                        // 選択したBrickを押す
                        $scope.Puzzle._BrickPress(brickToPush.BrickIdx, 1);

                        // Brickがすでに押されていた場合、何もしない
                        if ($scope.Puzzle.DataSet[brickToPush.BrickIdx].Owner != 1)
                            return;

                        // Brickがマッチングされた場合, BrainからペアBrickを削除 (存在している場合)
                        if ($scope.Puzzle.DataSet[brickToPush.BrickIdx].IsSolved) {
                            let indexFound = -1;
                            for (let i = 0; i < $scope.Puzzle.Players[1].Parameters.Brain.length; i++) {
                                if ($scope.Puzzle.Players[1].Parameters.Brain[i].Id == brickToPush.Id) {
                                    indexFound = i;
                                    break;
                                }
                            }
                            if (indexFound >= 0)
                                $scope.Puzzle.Players[1].Parameters.Brain.splice(indexFound, 1);

                            return;
                        }

                        // マッチング失敗の場合、Brainの中にマッチングするBrickがあるか確認
                        if ($scope.Puzzle.Players[1].Parameters.Brain.length > 0) {
                            let indexFound = -1;

                            for (let i = 0; i < $scope.Puzzle.Players[1].Parameters.Brain.length; i++) {
                                if ($scope.Puzzle.Players[1].Parameters.Brain[i].Id == brickToPush.Id) {
                                    indexFound = i;
                                    break;
                                }
                            }

                            if (indexFound >= 0) {
                                // マッチングするBrickがある場合、SetFoundに記憶。Brainからは削除
                                $scope.Puzzle.Players[1].Parameters.SetFound.push($scope.Puzzle.Players[1].Parameters.Brain[indexFound], { Id: brickToPush.Id, BrickIdx: brickToPush.BrickIdx })
                                $scope.Puzzle.Players[1].Parameters.Brain.splice(indexFound, 1);
                            }
                            else {
                                // マッチングするBrickがない場合、Brainに記憶。BrainがFullの場合、何もしない。
                                if ($scope.Puzzle.Players[1].Parameters.Brain.length == $scope.Puzzle.Players[1].Parameters.BrainSize) { }
                                else
                                    $scope.Puzzle.Players[1].Parameters.Brain.push({ Id: brickToPush.Id, BrickIdx: brickToPush.BrickIdx });
                            }
                        }
                        else {
                            // BrainがEmptyの場合、Brainに記憶。BrainがFullの場合、何もしない。
                            if ($scope.Puzzle.Players[1].Parameters.Brain.length == $scope.Puzzle.Players[1].Parameters.BrainSize) { }
                            else
                                $scope.Puzzle.Players[1].Parameters.Brain.push({ Id: brickToPush.Id, BrickIdx: brickToPush.BrickIdx });
                        }
                    };
                }
            },
            Interval: undefined        
            }],
        // ゲーム初期化
        Init: function () {
            $scope.Puzzle.Status.GameProgress = "Ready";
        },
        // プレイア設定ロード
        PlayersLoad: function () {
            for (let i = 0; i < $scope.Puzzle.Players.length; i++) {
                $scope.Puzzle.Players[i].Status.Score = 0;
            }
        },
        // ゲームロード
        GameLoad: function () {
            // 1. ゲームLoad
            // 1.1) データベースのデータを利用し、マッチングゲームのデータを生成
            $scope.Database.Wrestler.sort(function (a, b) { return 0.5 - Math.random() });

            let maxPuzzleLength = 8;
            if ($scope.Database.Wrestler.length < 8)
                maxPuzzleLength = $scope.Database.Wrestler.length;

            $scope.Puzzle.DataSet = [];
            for (let i = 0; i < maxPuzzleLength; i++) {
                // A セット
                $scope.Puzzle.DataSet.push(
                    {
                        Id: $scope.Database.Wrestler[i].Idx,
                        Image: $scope.Database.Wrestler[i].A,
                        DisplayImage: "img/question.jpg",
                        Owner: null,            // Brickを押したユーザー
                        IsUnreachable: false,   // Brickが押せない状態であるか
                        IsSolved: false         // Brickの状態 - false: マッチング未完了, true: マッチング完了
                    }
                );
                // B セット
                $scope.Puzzle.DataSet.push(
                    {
                        Id: $scope.Database.Wrestler[i].Idx,
                        Image: $scope.Database.Wrestler[i].B,
                        DisplayImage: "img/question.jpg",
                        Owner: null,
                        IsUnreachable: false,
                        IsSolved: false
                    }
                );
            }

            // 1.2) データをShuffle
            $scope.Puzzle.DataSet.sort(function (a, b) { return 0.5 - Math.random() });

            // 2. システムAIの設定
            $scope.Puzzle.System.Parameters.Speed = 1000;
        },
        // ゲームスタート
        GameStart: function () {
            // 1. プレイアLoad
            $scope.Puzzle.PlayersLoad();

            // 2. ゲームLoad
            $scope.Puzzle.GameLoad();

            // 3. ゲーム開始
            alert("Game Start !");

            // 3.1) システムAI起動
            $scope.Puzzle.Status.GameProgress = "Playing";
            $scope.Puzzle.System.Interval = $interval($scope.Puzzle.System.Action, $scope.Puzzle.System.Parameters.Speed);

            // 3.2) CPU起動
            $scope.Puzzle.Players[1].Interval = $interval($scope.Puzzle.Players[1].Action.Routine, $scope.Puzzle.Players[1].Parameters.Speed);
        },
        // Brickを押すイベント処理
        _BrickPress: function (brickIdx, owner) {
            let pushedBrick;
            pushedBrick = $scope.Puzzle.DataSet[brickIdx];

            // 1. すでに押されている場合、return
            if (pushedBrick.Owner != null)
                return;
              
            // 2. 押されていない場合の処理
            pushedBrick.DisplayImage = pushedBrick.Image;
            pushedBrick.Owner = owner;

            // 1Turn前に押したBrick
            let selectedBrick;

	        for (let i = 0; i < $scope.Puzzle.DataSet.length; i++) {
		        if (i != brickIdx && $scope.Puzzle.DataSet[i].IsUnreachable == false && $scope.Puzzle.DataSet[i].IsSolved == false && $scope.Puzzle.DataSet[i].Owner == owner)
			    selectedBrick = $scope.Puzzle.DataSet[i];
	        }

            // 1Turn前に押したBrickがある場合
            if (selectedBrick != undefined) {
                
                if (selectedBrick.Id == pushedBrick.Id) {

                    // マッチング成功
                    selectedBrick.IsSolved = true;
                    pushedBrick.IsSolved = true;

                    // 点数設定
                    for (let i = 0; i < $scope.Puzzle.Players.length; i++) {
                        if (i == owner)
                            $scope.Puzzle.Players[i].Status.Score++;
                    }
                }
                else {

                    // マッチング失敗
                    selectedBrick.IsUnreachable = true;
                    pushedBrick.IsUnreachable = true;

                    $timeout(function () {
                        selectedBrick.DisplayImage = "img/question.jpg";
                        selectedBrick.Owner = null;
                        selectedBrick.IsUnreachable = false;

                        pushedBrick.DisplayImage = "img/question.jpg";
                        pushedBrick.Owner = null;
                        pushedBrick.IsUnreachable = false;
                    }, 750);
                }
            }            
        },
        // Brickの色処理
        _BrickColor: function (owner, isMatched) {
            let color = "white";

            if (owner == null) { }
            else if (isMatched)
                color = $scope.Puzzle.Players[owner].Parameters.BrickColorSolved;
            else
                color = $scope.Puzzle.Players[owner].Parameters.BrickColorPressed;

            return color;
        }
    };

    // プログラム起動
    $scope.Main.Init();
});