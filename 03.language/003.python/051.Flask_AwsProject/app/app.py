from flask import Flask, render_template, request
from werkzeug.exceptions import HTTPException
from pcj.view import home, manage
from pcj.model import dbManager

import atexit
import logging
import traceback

app = Flask(__name__)

# Make the WSGI interface available at the top level so wfastcgi can get it.
wsgi_app = app.wsgi_app

#--------------------------------------------------------------------------
# config - basic (logging is first)
#--------------------------------------------------------------------------
app.logger.setLevel(logging.INFO)
app.config.from_object("config")

#--------------------------------------------------------------------------
# config - logging
#--------------------------------------------------------------------------
app.logger.setLevel(app.config["LOGGINGLEVEL"])

#--------------------------------------------------------------------------
# config - database
#--------------------------------------------------------------------------
dbManager.initDb(app)

#--------------------------------------------------------------------------
# routing - common
#--------------------------------------------------------------------------
# 1. root
@app.route('/')
def root():
    return "ok"

# 2. error - 403
@app.errorhandler(403)
def forbidden(e):
    return render_template("status403.html"), 403

# 3. error - 404
@app.errorhandler(404)
def page_not_found(e):
    return render_template("status404.html"), 404

# 4. error - other http status (except for 404, include 500)
@app.errorhandler(HTTPException)
def handle_httpException():
    app.logger.error("HTTPException - url: {}, method: {}, clientIp: {}".format(request.url, request.method, request.access_route))
    app.logger.error(traceback.format_exc())
    return render_template("Error.html"), e.get_response().status_code

# 5. error - unhandled exception
@app.errorhandler(Exception)
def handle_exception():
    app.logger.error("Exception - url: {}, method: {}, clientIp: {}".format(request.url, request.method, request.access_route))
    app.logger.error(traceback.format_exc())
    return render_template("Error.html"), e.get_response().status_code

#--------------------------------------------------------------------------
# routing - blue print
#--------------------------------------------------------------------------
app.register_blueprint(home.bp)
app.register_blueprint(manage.bp)

#--------------------------------------------------------------------------
# server start
#--------------------------------------------------------------------------
if __name__ == '__main__':
    import os
    HOST = os.environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(os.environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555

    app.logger.info("app started.")
    app.run(HOST, PORT)

#--------------------------------------------------------------------------
# server stop
#--------------------------------------------------------------------------
def appStop():
    app.logger.info("app stopped.")
atexit.register(appStop)