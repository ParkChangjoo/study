from flask import Blueprint, redirect, url_for, render_template, request, abort, make_response, session

bp = Blueprint("home", __name__, url_prefix="/home")

@bp.route("/")
def root():
    return render_template("home.html")

@bp.route("/game")
def game():
    return render_template("game.html")