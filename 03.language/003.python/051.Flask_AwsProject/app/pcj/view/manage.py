from flask import Blueprint, redirect, url_for, render_template, request, abort, make_response, session, flash
from flask import current_app
from werkzeug.security import generate_password_hash, check_password_hash
from pcj.model import dbManager
from pcj.decorator import login_required, Authorize, AntiCSRF
import traceback

bp = Blueprint("manage", __name__, url_prefix="/manage")

@bp.route("/")
@login_required
def root():
    return "manage"

@bp.route("/login", methods=["GET","POST"])
@AntiCSRF
def login():
    if request.method == "GET":
        return render_template("manage/login.html")
    else:
        name = request.form["name"]
        pswd = request.form["pass"]

        current_app.logger.info(name)
        current_app.logger.info(pswd)
        current_app.logger.info(generate_password_hash(pswd))

        con = dbManager.getConnection()
        cur = con.cursor()

        try:
            sql = "select * from nandemo_manage.user where name = %s"
            val = (name,)
            cur.execute(sql, val)
            result = cur.fetchone()

            if result is None:
                flash("login failed")
                print(1)
                return render_template("manage/login.html")
            else:
                if check_password_hash(result[2], pswd):
                    print(2)
                    session["name"] = result[1]
                    session["role"] = result[3]
                    return redirect(url_for("manage.home"))
                else:
                    flash("login failed")
                    print(3)
                    return redirect(url_for("manage.login"))
        except Exception as e:
            msg = "Error:{}".format(e)
            flash("Error:{}".format(traceback.format_exc()))
            return redirect(url_for("manage.login"))
        finally:
            cur.close()
            dbManager.closeConnection(con)

@bp.route("/logout", methods=["POST"])
@login_required
@AntiCSRF
def logout():
    session.clear()
    return redirect(url_for("manage.login"))

@bp.route("/register", methods=["GET","POST"])
@login_required
@Authorize("admin")
@AntiCSRF
def register():
    if request.method == "GET":
        return render_template("manage/register.html")
    else:
        name = request.form["name"]
        pswd = request.form["pass"]

        current_app.logger.info(name)
        current_app.logger.info(pswd)

        con = dbManager.getConnection()
        cur = con.cursor()

        try:
            sql = "select count(*) cnt from nandemo_manage.user where name = %s"
            val = (name,)
            cur.execute(sql, val)
            result = cur.fetchone()

            if result[0] > 0:
                flash("name already exist")
                return redirect(url_for("manage.register"))

            sql = "insert into nandemo_manage.user(name, password) values (%s, %s)"
            val = (name, generate_password_hash(pswd))

            cur.execute(sql, val)
            con.commit()
            return redirect(url_for("manage.home"))
        except Exception as e:
            con.rollback()
            msg = "Error:{}".format(e)
            flash("Error:{}".format(traceback.format_exc()))
            return redirect(url_for("manage.register"))
        finally:
            cur.close()
            dbManager.closeConnection(con)

@bp.route("/home", methods=["GET"])
@login_required
@AntiCSRF
def home():
    name = session["name"]
    return render_template("manage/home.html", name=name)

@bp.route("/first", methods=["GET"])
@login_required
@Authorize("admin")
@AntiCSRF
def first():
    name = session["name"]
    return render_template("manage/first.html", name=name)

@bp.route("/second", methods=["GET"])
@login_required
@Authorize("admin","engineer")
@AntiCSRF
def second():
    name = session["name"]
    return render_template("manage/second.html", name=name)

@bp.route("/third", methods=["GET"])
@login_required
@Authorize("admin","engineer","qa","operator")
@AntiCSRF
def third():
    name = session["name"]
    return render_template("manage/third.html", name=name)