from functools import wraps
from flask import current_app, session, redirect, url_for, g, request
from werkzeug.exceptions import abort

import secrets

def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if "role" in session:
            pass
        else:
            current_app.logger.info("login_required. fail")
            return redirect(url_for("manage.login"))

        return func(*args, **kwargs)

    return wrapper

def Authorize(*roleNames):
    def Decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if "role" in session:
                pass
            else:
                current_app.logger.info("Authorize. fail")
                abort(403)

            role = session["role"]
            if role in roleNames:
                pass
            else:
                current_app.logger.info("Authorize. fail")
                abort(403)

            return func(*args, **kwargs)

        return wrapper

    return Decorator

def AntiCSRF(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if request.method == "GET":
            token = secrets.token_urlsafe(16)
            session["CSRFToken"] = token
            g.CSRFToken = token
            return func(*args, **kwargs)
        elif request.method == "POST":
            tokenSubmitted = request.form["CSRFToken"]
            if tokenSubmitted is None:
                current_app.logger.info("Blocked CSRF attack")
                abort(403)

            token = session["CSRFToken"]
            if token != tokenSubmitted:
                current_app.logger.info("Blocked CSRF attack")
                abort(403)

            return func(*args, **kwargs)
        else:
            abort(405)

    return wrapper