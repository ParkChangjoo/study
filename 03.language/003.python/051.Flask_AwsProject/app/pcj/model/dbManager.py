import mysql.connector
import mysql.connector.pooling
from flask import current_app

def initDb(app):
    dbConfig = app.config["DBCONFIG"]
    pool = mysql.connector.pooling.MySQLConnectionPool(
        host=dbConfig["host"],
        user=dbConfig["user"],
        passwd=dbConfig["passwd"],
        pool_name="nandemo_pool",
        pool_size=dbConfig["pool_size"]
    )
    app.config["dbConnectionPool"] = pool

def getConnection():
    con = current_app.config["dbConnectionPool"].get_connection()
    return con

def closeConnection(con):
    con.close()
