----------------------------------
local execution
----------------------------------
. activate
export FLASK_ENV=development #for debugging
python3 app.py

----------------------------------
build (create a zip file)
----------------------------------
sh build.sh environment

----------------------------------
deploy to elasticbeanstalk
----------------------------------
upload zip

----------------------------------
log files
----------------------------------
1. access log: /var/log/httpd/access_log
2. log: /var/log/httpd/error_log

----------------------------------
database scheme
----------------------------------
CREATE DATABASE `nandemo_manage` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;
CREATE TABLE `user` (
  `idx` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `role` varchar(20) NOT NULL DEFAULT 'operator',
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

----------------------------------
To do (뭐든지 외우자 서버)
----------------------------------
0. 레이아웃
	데이터 엑세스
		soc
			template
			static
			decorator
		확장성 고려 (서버를 옆으로 늘릴 수 있는가): cookie-based session (default. cookie + encryption)

1. 인증, 권한
	db - manage.user
	authentication
		session
	authorization
		decorator.login_required
		decorator.Authorize

2. 플로어 제어
	application start, stop
		console log
	exception handling
		403, 404, error page
	logging
		console log
			development: debug level
			production: error level

3. 암호화 (비밀번호)
	password hashing: werkzeug.security.generate_passworkd_hash

4. 해킹방지 (sql 인젝션, 크로스사이트스크립팅, CSRFs)
	sql 인젝션: named parameter
	크로스사이트스크립팅: jinja2 autoescaping is enabled by flask (default)
	CSRFs: decorator.AntiCSRF

5. config file
	config.py

6. api 서버 만들기 (rest)
7. unit test

db connection 해결 (스태틱? 글로벌 인스턴스?)