env=$1

# copy config file by environment
if ["${env}" = "development"]; then
	echo "environment: development"
	cp config/dev.py app/config.py
elif ["${env}" = "production"]; then
	echo "environment: development"
	cp config/prod.py app/config.py
else
	echo "ERROR: environment is null"
	exit
fi

zip -r app/build.zip app