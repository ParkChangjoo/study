
import mysql.connector
import mysql.connector.pooling

# get connection
connection = mysql.connector.connect(
    host="127.0.0.1",
    user="root",
    passwd="1234"
)

# get cursor
cursor = connection.cursor()

try:
    # select
    # - fetchall : get all rows
    # - fetchone : get top 1 row
    # - cursor.execute(sql, val) : prevent sql injection
    sql = "select * from wrestling.wrestler where name = %s"
    val = ("Kojet",) # tuple
    cursor.execute(sql, val)
    result = cursor.fetchall()
    print(result)
except mysql.connector.Error as err:
    print(err)
    connection.rollback()
except Exception as e:
    print(e)
    connection.rollback()
finally:
    # close connection
    cursor.close()
    connection.close()

# get connection pool
cnxpool = mysql.connector.pooling.MySQLConnectionPool(
    host ="127.0.0.1",
    user = "root",
    passwd = "1234",
    pool_name = "mypool",
    pool_size = 3
)
cnx1 = cnxpool.get_connection()
cnx1.close()