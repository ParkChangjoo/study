# open file
#
# r - read.                 error when file not exists.     default
# a - write (append).       create file.
# w - write (overwrite).    create file.
# x - create file.          error when file exists.
#
# t - text mode.            default.
# b - binary mode.

# create dir
if os.path.exists("myFiles/subdir"):
    print("dir already exists.")
    pass
else:
    os.makedirs("myFiles/subdir")
    print("dir created")

# create file
try:
    f = open("myFiles/subdir/kingkong.txt", "xt")
    print("file created.")
except:
    print("file already exists.")
finally:
    f.close()

# write (overwrite)
f = open("myFiles/subdir/kingkong.txt", "wt")
content = """King Kong is strong.
Very very strong.
And awesome."""
f.write(content)
f.close()

f = open("myFiles/subdir/kingkong.txt", "rt")
content = f.read()
print(content)
f.close()

# write (append)
f = open("myFiles/subdir/kingkong.txt", "at")
f.write("I am kingkong.")
f.close()

f = open("myFiles/subdir/kingkong.txt", "rt")
content = f.read()
print(content)
f.close()

# read all
f = open("myFiles/subdir/kingkong.txt", "rt")
content = f.read()
print(content)

# read line
f = open("myFiles/subdir/kingkong.txt", "rt")
line = f.readline()
print(line)

# read line (first 2 lines)
f = open("myFiles/subdir/kingkong.txt", "rt")
line = f.readline()
print(line)
line = f.readline()
print(line)

# read line (loop)
f = open("myFiles/subdir/kingkong.txt", "rt")
for line in f:
    print(line)

# close file
f.close()

# remove file. check exists.
if os.path.exists("myFiles/subdir/kingkong.txt"):
    os.remove("myFiles/subdir/kingkong.txt")
    print("file removed")
else:
    print("no file")

# remove dir
if os.path.exists("myFiles/subdir"):
    os.rmdir("myFiles/subdir")
    os.rmdir("myFiles")
    print("dir removed")
else:
    pass