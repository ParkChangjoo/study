# for (x = 0; x < 3; x++)
for x in range(3):
	print(x)

for x in range(0, 3):
	print(x)

# foreach
myList = [10, 20, 30]
for x in myList:
	print(x)

# break
for x in myList:
	if x == 30:
		break
	print(x)

# continue
for x in myList:
	if x == 10:
		continue
	print(x)

# while
a = 0
while a < 3:
	print(a)
	a = a + 1