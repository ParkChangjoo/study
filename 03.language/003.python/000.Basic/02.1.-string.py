# multi line
d = """kingkong
is big
and
strong"""
print(d)

# length
lenD = len(d)
print(lenD)

# lower, upper case
lowerD = d.lower()
uppperD = d.upper()
print(lowerD)
print(uppperD)

# replace
replaceD = d.replace("k", "!")
print(replaceD)

# substring
d = "King,Kong"
d0 = d[0]
print(d[0])
d0To1 = d[0:2] # start index : end index - 1
print(d0To1)
d0To1 = d[:2] # null : end index - 1
print(d0To1)
d0ToEnd = d[0:] # start index : null
print(d0ToEnd)

# split
splitD = d.split(",")
print(splitD[0])
print(splitD[1])

# format
e = "kingkong is {}"
formatE = e.format("strong")
print(formatE)
e = "kingkong is {}, {}, {}"
formatE = e.format("strong", 10, 20)
print(formatE)

# in, not in
isInD = "123" in d
print(isInD)
isNotInD = "123" not in d
print(isNotInD)