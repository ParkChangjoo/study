# create
class myClass:
	x = 5

instance1 = myClass()
print(instance1.x)

# constructor
class Person:
	def __init__(self, name, age):
		self.name = name
		self.age = age

john = Person("John", 15)
print(john.name)
print(john.age)

# method
class Animal:
	def __init__(self, name, age):
		self.name = name
		self.age = age

	def say(self):
		print(self.name, self.age)

kingkong = Animal("kingkong", 10)
kingkong.say()