import json

# json -> python dic
jsonString = '{"name":"John", "age":10}'
pythonDic = json.loads(jsonString)
print(pythonDic["name"])

# python dic -> json
pythonDic = {
	"name":"kingkong",
	"age":10
}
jsonString = json.dumps(pythonDic)
print(jsonString)

# python dic -> json (easy to read)
jsonString = json.dumps(pythonDic, indent=4)
print(jsonString)

# python dic -> json (sort by python dic key)
jsonString = json.dumps(pythonDic, indent=4, sort_keys=True)
print(jsonString)