# create package
# https://docs.python.org/3/tutorial/modules.
# [create]
# 1. Create package
# 2. Create module inside package
# 3. Add root folder (01.Basic) to Search Paths (검색 경로)
# 
# [use]
# from package import module
from myPackage.util import myModule

# use module property
sum = myModule.x + myModule.y
print(sum)

myModule.printSum(5, 10)