a = 1
b = 1

# basic
if a == b:
	print(True)
elif a > b:
	print(True)
else:
	print(True)

# and, or
if a == b and a == b:
	print(True)
elif a > b or a < b:
	print(True)
else:
	print(True)

# nested
if a == b:
	if a == b:
		print(True)
	else:
		print(True)
else:
	print(True)

# do nothing
if a > b:
	pass
else:
	print(False)
