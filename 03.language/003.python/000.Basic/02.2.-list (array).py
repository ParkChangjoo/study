﻿# ordered. changeable. indexed. allow duplicate. array. ex) x = ["a", "b", "c"]
# create list
myList = ["a","b","c","d","e"]
print(myList)

# sub list
myList0To1 = myList[0:2] # start index : end index - 1
print(myList0To1)
myList0To1 = myList[:2] # null : end index - 1
print(myList0To1)
myList0ToEnd = myList[0:] # start index : null
print(myList0ToEnd)

# append
myList.append("f")
print(myList)

# append with index
myList.insert(1, "a2")
print(myList)

# remove
myList.remove("a2")
print(myList)

# remove with index
del myList[5]
print(myList)

# clear
myList.clear()
print(myList)

# copy
myList2Ref = myList
myList2Copy = myList.copy()
myList.append("f")
print(myList2Ref)
print(myList2Copy)

# concat 2 list
myListA = ["1","2"]
myListB = ["3","4"]
myList = myListA + myListB
print(myList)

# concat 3 list
myListA = [1,2]
myListB = [3,4]
myList = myListA + myListB
print(myList)