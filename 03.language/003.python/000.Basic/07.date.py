from datetime import datetime, timezone, timedelta

# now
now = datetime.now()
nowUtc = datetime.utcnow()

print(now)
print(nowUtc)
print(nowUtc.year)
print(nowUtc.month)
print(nowUtc.day)
print(nowUtc.hour) # return 24 hour
print(nowUtc.minute)
print(nowUtc.second)
print(nowUtc.microsecond)
print(nowUtc.tzinfo)

# create
myDate = datetime(2020, 1, 2, 3, 4, 5, 6)
print(myDate)

# create with timezone
myDateUtc = datetime(2020, 1, 2, 3, 4, 5, 6, tzinfo=timezone.utc)
print(myDateUtc)

# format - https://docs.python.org/3.2/library/time.html
yyyymmdd = myDate.strftime("%Y-%m-%d %H:%M:%S.%f")
print(yyyymmdd)

# add
minuteToAdd = timedelta(minutes=1)
myDate = myDate + minuteToAdd
print(myDate)

timeToAdd = timedelta(weeks=1, days=1, hours=1, minutes=1, seconds=1, milliseconds=1, microseconds=1)
myDate = myDate + timeToAdd
print(myDate)