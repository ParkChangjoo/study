# unordered. changeable. indexed. no duplicate. ex) x = {"name":"kingkong", "age":10}
# create
myDic = {"name":"kingkong", "age":10}
myDic = {
	"name":"kingkong",
	"age":10
}
myDic = dict(name="kingkong", age=10)
print(myDic)

# get value
name = myDic["name"]
print(name)

# set value
myDic["name"] = "hulk"
print(myDic["name"])

# length
length = len(myDic)
print(length)

# add item
myDic["new1"] = "new1"
print(myDic)

# delete item
del myDic["new1"]
print(myDic)

# loop 1
for x in myDic:
	key = x
	value = myDic[key]
	print(key, value)

# loop 2
for key, value in myDic.items():
	print(key, value)

# check item exists
if "name" in myDic:
	print(True)

# copy dictionary
myDic2Ref = myDic
myDic2Copy = myDic.copy()
myDic["new2"] = "new2"
print(myDic2Ref)
print(myDic2Copy)

# clear dictionary
myDic.clear()
print(myDic)