import traceback

try:
    print(1)
    raise Exception("Exception message: kingkong")
except Exception as e:
    print(e)
    traceback.print_exc() # stacktrace
finally:
    print(3)
