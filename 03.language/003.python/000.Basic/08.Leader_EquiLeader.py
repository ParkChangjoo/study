def solution(A):
    # 1. find leader
    dic = {}
    for val in A:
        if val in dic:
            dic[val] = dic[val] + 1
        else:
            dic[val] = 1
    
    leader = None
    leaderCount = 1
    for key, value in dic.items():
        if value > leaderCount and value >= len(A)/2:
            leader = key
            leaderCount = value
        else:
            continue
        
    # 2. count equi leader
    eqLCnt = 0
    for index in range(1,len(A)):
        lSlice = A[:index]
        rSlice = A[index:]
        
        lOk = lSlice.count(leader) > len(lSlice)/2
        rOk = rSlice.count(leader) > len(rSlice)/2
        
        if lOk and rOk:
            eqLCnt = eqLCnt + 1
        
    # 3. return equi count
    return eqLCnt

A = [4, 3, 4, 4, 4, 2]
result = solution(A)
print(result)