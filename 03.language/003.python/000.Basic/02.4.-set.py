# unordered. unchangeable. unindexed. no duplicate. ex) x = {"aaple","banana","cherry"}
# create
mySet = {"1","2","3"}
print(mySet)

# get value - only by loop
# set value - set is unchangable

# length
length = len(mySet)
print(length)

# add 1 item
mySet.add("4")
print(mySet)

# add multiple items
mySet.update(["5","6"])
print(mySet)

# loop
for x in mySet:
	print(x)

# check item exist
if "1" in mySet:
	print(True)

print("--------------------------")
mySet2 = {"6","7","8"}

# find duplicated items
inter = mySet.intersection(mySet2)
print(inter)

# find not duplicated items
diff = mySet.difference(mySet2)
print(diff)

# union (return new set)
# - !!: duplicated item is removed.
joinSet = mySet.union(mySet2)
print(mySet)
print(joinSet)

# update (insert new items)
# - !!: duplicated item is removed.
# - insert items to set
mySet.update(mySet2)
print(mySet)

print("----------------------------")
# delete item
mySet.remove("6")
print(mySet)

# delete last item
# - set is unordered, so you don't know what item will be deleted
mySet.pop()
print(mySet)

# clear
mySet.clear()
print(mySet)

# del: delete set
del mySet
print(mySet)