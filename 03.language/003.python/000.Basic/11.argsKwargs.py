# *:    one * means multiple arguments -> handled as tuple
# **:   two ** means multiple arguments -> handled as dictionary

def helloArgs(*args):
    for x in args:
        print(x)

helloArgs("apple", "banana", "orange")

def helloKwargs(**kwargs):
    for key, value in kwargs.items():
        print(key, value)

helloKwargs(name="kingkong", age=10)