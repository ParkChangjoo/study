# create decorator - for function without return
def myDecorator(func):
    def wrapper(*args, **kwargs):
        print("Before func")
        func(*args, **kwargs)
        print("After func")
    
    return wrapper

@myDecorator
def helloWorld():
    print("Hello, world")

helloWorld()


# create decorator - for function with or without return
# 1) no return
def myDecorator2(func):
    def wrapper(*args, **kwargs):
        print("Before func")
        rtn = func(*args, **kwargs)
        print("After func")
        return rtn

    return wrapper

@myDecorator2
def noReturn():
    print("Hello, world")

noReturn()

# 2) return
@myDecorator2
def withReturn():
    return "Hello, world"

result = withReturn()
print(result)


# create decorator - for function with or without return + parameter
# 1) no return
def myDecorator3(name):
    def innerDecorator(func):
        def wrapper(*args, **kwargs):
            print("Before func:" + name)
            rtn = func(*args, **kwargs)
            print("After func:" + name)
            return rtn

        return wrapper

    return innerDecorator

@myDecorator3("kingkong")
def hi():
    print("hi")

hi()

# 2. return
@myDecorator3("kingkong")
def hello():
    return "hello"

result = hello()
print(result)