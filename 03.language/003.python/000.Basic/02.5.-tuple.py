# ordered. unchangeable. indexed. allow duplicate. ex) x = ("a","b","c")
# create
myTuple = ("apple","banana","cherry")
print(myTuple)

myTuple2 = ("apple",) # only 1 item
print(myTuple2)

# get value
print(myTuple[0])
print(myTuple[0:2])

# set value - tuple is unchangable

# length
length = len(myTuple)
print(length)

# add item - tuple is unchangable
# delete item - tuple is unchangable

# loop
for x in myTuple:
	print(x)

# check item exist
if "apple" in myTuple:
	print(True)

# clear - tuple is unchangable