#1. print
print("--------------------------------")
print("Hello, World")

#2. variable
print("--------------------------------")
# variable is case sensitive.
a = 1
b = "hello"

print(a)
print(b)

#3. block of code
print("--------------------------------")
if a == 1:
	print("ok")

#4. function
print("--------------------------------")
def printHello():
	print("Hello")

printHello()

#5. data type
# text
#	string		ex) x = "Hello, world"
# numeric
#	int			ex) x = 1
#	float		ex) x = 1.5
#	complex		?
# sequence
#	list		ordered. changeable. indexed. allow duplicate. array. ex) x = ["a", "b", "c"]
#	tuple		ordered. unchangeable. indexed. allow duplicate. ex) x = ("a","b","c")
#	range		used for loop. ex) for x in range(3) or range(0,3) = 0,1,2
# mapping
#	dict		unordered. changeable. indexed. no duplicate. ex) x = {"name":"kingkong", "age":10}
# set
#	set			unordered. unchangeable. unindexed. no duplicate. ex) x = {"aaple","banana","cherry"}
#	fronzenset	?
# boolean
#	bool		ex) x = True (True, False)
# binary
#	bytes		ex) x = b"Hello"
#	bytearray	ex) x = bytearray(5)
#	memoryview

# type
print("--------------------------------")
c = 1
typeOfC = type(c)
print(typeOfC)

# numeric
print("--------------------------------")
f = 1
print(f)
f = float(f)
print(f)
f = int(f)
print(f)
f = str(f)
print(f)

# boolean
print("--------------------------------")
print(10 > 9)
print(10 < 9)
print(10 == 10)

false1 = bool(0)
false2 = bool("")
false3 = bool([])
print(false1)
print(false2)
print(false3)