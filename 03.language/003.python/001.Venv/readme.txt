1. python3 -m venv folderName
- create folder with folderName
- create virtual environment files in the folder

2. cd folderName/bin
3. . activate
- activate virtual environment
4. do work
5. deactivate
- deactivate virtual environment