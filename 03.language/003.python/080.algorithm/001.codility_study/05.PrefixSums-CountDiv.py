def solution(A, B, K):
    # write your code in Python 3.6
    numbers = list()
    
    for number in range(A, B+1):
        print(number)
        if number % K == 0:
            numbers.append(number)
            
    print(numbers)
    return len(numbers)

A = 0
B = 0
C = 1
result = solution(A,B,C)
print(result)