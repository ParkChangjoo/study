def solution(A):
    # write your code in Python 3.6
    # remove duplication
    setA = set(A)
    distinctCount = len(setA)
    return distinctCount

A = [2,1,1,2,3,1]
result = solution(A)
print(result)