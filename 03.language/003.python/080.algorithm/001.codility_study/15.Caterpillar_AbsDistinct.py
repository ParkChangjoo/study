def solution(A):
    newList = list()
    for val in A:
        newList.append(abs(val))
        
    newSet = set(newList)
    return len(newSet)

A = [-5,-3,-1,0,3,6]
result = solution(A)
print(result)