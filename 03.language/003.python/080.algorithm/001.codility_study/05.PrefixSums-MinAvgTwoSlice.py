# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(A):
    # write your code in Python 3.6
    # update minAvg, minAvgStartIndex in the loop
    minAvg = 10000
    minAvgStartIdx = -1
    
    for startIdx in range(0, len(A)-1):
        for endIdx in range(startIdx+1, len(A)):
            slice = A[startIdx:endIdx+1]
            avg = sum(slice)/len(slice)

            if avg < minAvg:
                minAvgStartIdx = startIdx
                minAvg = avg
            
    return minAvgStartIdx

A = [4, 2, 2, 5, 1, 5, 8]
result = solution(A)
print(result)