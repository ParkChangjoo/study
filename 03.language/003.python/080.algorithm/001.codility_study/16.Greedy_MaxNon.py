def solution(A, B):
    # write your code in Python 3.6
    maxSeg = -1
    maxSegIdx = 0
    # pick 1 then compare to the others if it is overlapped
    # if not, compare maxSeg to previous one
    # if maxSeg is greater than previous one, update maxSeg, maxSegIdx
    for myIdx in range(0,len(A)):
        for chkIdx in range(myIdx, len(A)):
            myMin = A[myIdx]
            myMax = B[myIdx]
            chkMin = A[chkIdx]
            chkMax = B[chkIdx]
            
            if myMin < chkMax or myMax > chkMin:
                continue
            else:
                if maxSeg < myMax:
                    maxSeg = myMax
                    maxSegIdx = myIdx
                    
    return maxSegIdx

A = [0]
B = [0]
result = solution(A,B)
print(result)