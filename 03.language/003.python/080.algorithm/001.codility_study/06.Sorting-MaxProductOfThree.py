def solution(A):
    # write your code in Python 3.6
    # update maxProduct in loop
    maxProduct = -1000*-1000*-1000
    for pIdx in range(0,len(A)):
        for qIdx in range(pIdx+1,len(A)):
            for rIdx in range(qIdx+1,len(A)):
                product = A[pIdx]*A[qIdx]*A[rIdx]
                if product > maxProduct:
                    maxProduct = product
    
    return maxProduct

A = [-3,1,2,-2,5,6]
result = solution(A)
print(result)