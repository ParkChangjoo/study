def solution(A):
    resultList = list()
    for val in A:
        nd = list()
        for candidate in A:
            if val % candidate == 0:
                pass
            else:
                nd.append(val)
        resultList.append(len(nd))
        
    return resultList

A = [1,2,5,7,10]
result = solution(A)
print(result)