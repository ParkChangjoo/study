def solution(A):
    # cut prefix part
    bVal = bin(A)
    bVal = bVal[2:]
    
    # create splitedSet
    splitedVal = bVal.split("1")
    # find max binary gap
    maxGap = 0
    for idx in range(1,len(splitedVal) - 1):
        val = splitedVal[idx]
        if len(val) > maxGap:
            maxGap = len(val)
        else:
            continue
        
    return maxGap

A = 32
result = solution(A)
print(result)