def solution(A):
    # write your code in Python 3.6
    # find minimum diff shifting index position to right
    # P = 1
    leftArraySum = sum(A[:1])
    rightArraySum = sum(A[1:])
    diff = abs(leftArraySum - rightArraySum)
    
    # P > 1
    for idx in range(1,len(A)-1):
        leftArraySum = leftArraySum + A[idx]
        rightArraySum = rightArraySum - A[idx]
        tempDiff = abs(leftArraySum - rightArraySum)
        if diff > tempDiff:
            diff = tempDiff
            
    return diff

A = [-5,-3,0,3,5]
result = solution(A)
print(result)