def solution(N):
    # write your code in Python 3.6
    # if N % value == 0, then value is factor
    factorCount = 0
    val = 1
    while val <= N:
        if N%val == 0:
            factorCount = factorCount + 1
        else:
            pass
        
        val = val + 1
        
    return factorCount

N = 24
result = solution(N)
print(result)