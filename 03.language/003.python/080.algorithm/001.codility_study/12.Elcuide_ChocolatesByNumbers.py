def solution(N, M):
    # write your code in Python 3.6
    # chocolate count you eat
    chokoCnt = 0
    # positions where you eat chocolate
    dicWrapper = {}
    # start eating chocolate
    pos = 0
    while True:
        if pos in dicWrapper:
            break;
        chokoCnt = chokoCnt + 1
        dicWrapper[pos] = None
        pos = (pos + M)%N
        
    # return chocolate count you eat
    return chokoCnt

N = 1
M = 1
result = solution(N,M)
print(result)