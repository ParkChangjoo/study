# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")
stack = list()
fishSizeList = list()
fishFlowList = list()
def solution(A, B):
    global fishSizeList
    global fishFlowList
    fishSizeList = A.copy()
    fishFlowList = B.copy()
    
    # write your code in Python 3.6
    for fish in range(0,len(fishSizeList)):
        fight(fish)
    return len(stack)

def fight(fish):
    global stack
    if len(stack) == 0:
        stack.append(fish)
    else:
        stackFish = stack[-1]
        # check direction
        if fishFlowList[stackFish] == 1 and fishFlowList[fish] == 0:
            # check size
            if fishSizeList[stackFish] > fishSizeList[fish]:
                # fish dies
                pass
            else:
                # stackFish dies
                del stack[-1]
                # add fish into stack
                fight(fish)
        else:
            stack.append(fish)

K = [4, 3, 2, 1, 5]
L = [0, 1, 0, 0, 0]
result = solution(K, L)
print(result)
