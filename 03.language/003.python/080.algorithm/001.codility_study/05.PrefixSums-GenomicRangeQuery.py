# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(S, P, Q):
    # write your code in Python 3.6
    # impact factors and impacts
    iFactors = {"A":1,"C":2,"G":3,"T":4}
    # count of query
    qCount = len(P)
    
    # find minimum impact for each query
    resultList = list()
    for qIdx in range(0, qCount):
        minPos = P[qIdx]
        maxPos = Q[qIdx]
        minImpact = 4
        for sIdx in range(minPos,maxPos+1):
            factorName = S[sIdx]
            impact = iFactors[factorName]
            if impact < minImpact:
                minImpact = impact
        resultList.append(minImpact)
        
    return resultList

S = "A"
P = [0]
Q = [0]
result = solution(S,P,Q)
print(result)
