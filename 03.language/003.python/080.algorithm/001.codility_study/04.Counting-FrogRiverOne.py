def solution(X, A):
    # write your code in Python 3.6
            
    # find max time when posSet becomes empty
    posSet = set()
    for pos in range(X):
        posSet.add(pos+1)
        
    # find when posSet becomes empty
    time = 0
    for pos in A:
        if pos in posSet:
            posSet.remove(pos)
        
        if len(posSet) == 0:
            return time

        time = time + 1

    # return -1 if posSet is not empty
    if len(posSet) > 0:
        return -1

A = [1,2]
X = 2
result = solution(X, A)
print(result)