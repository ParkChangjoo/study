def solution(A):
    # write your code in Python 3.6
    A.sort()
    
    prevValue = 0
    for value in A:
        if value <= 0:
            continue
        offset = value - prevValue
        if offset > 1:
            return prevValue + 1
        else:
            prevValue = value
    
    return prevValue + 1

A = [1, 3, 6, 4, 1, 2]
result = solution(A)
print(result)