def solution(A):
    # write your code in Python 3.6
    # 1. create dictionary {"Value":"Index list"}
    dic = {}
    for idx in range(0,len(A)):
        if A[idx] in dic:
            dic[A[idx]].append(idx)
        else:
            idxList = list()
            idxList.append(idx)
            dic[A[idx]] = idxList
    
    # 2. find key with longest index list from dic
    tmp_key = 0
    tmp_length = 0
    for key, value in dic.items():
        if len(value) > tmp_length:
            tmp_key = key
            tmp_length = len(value)
        else:
            continue
    
    # 3. check if it is dominator, then return result
    if tmp_length > len(A)/2:
        result = dic[tmp_key][0]
        return result
    else:
        return -1

A = [3, 4, 3, 2, 3, -1, 3, 3]
result = solution(A)
print(result)
