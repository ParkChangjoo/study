def solution(S):
    # check if length is odd or even(pair)
    # Odd: 1, 3 ..
    isOdd = True
    if len(S) % 2 == 0:
        isOdd = False
    else:
        pass
    
    # create leftBuffer, rightBuffer
    if isOdd:
        middle = int(len(S)/2) + 1 # 7 -> 7/2 = 3 + 1 = 4
        leftBuffer = S[0:middle - 1] # 0,1,2
        rightBuffer = S[middle:] # 4,5,6 -> 6,5,4
        S = leftBuffer + rightBuffer

    # create stack
    stack = list()
    for val in S:
        if len(stack) == 0:
            stack.append(val)
        else:
            valInStack = stack[-1]
            if valInStack == "{" and val == "}":
                del stack[-1]
            elif valInStack == "(" and val == ")":
                del stack[-1]
            elif valInStack == "[" and val == "]":
                del stack[-1]
            else:
                stack.append(val)

    if len(stack) == 0:
        return 1
    else:
        return 0

A = "[{()()}]"
result = solution(A)
print(result)