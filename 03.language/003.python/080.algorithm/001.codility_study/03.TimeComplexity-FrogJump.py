import math

def solution(X, Y, D):
    # write your code in Python 3.6
    startPos = X
    toGoPos = Y
    jumpDistance = D
    
    timesToJump = (toGoPos - startPos)/jumpDistance
    timesToJump = math.ceil(timesToJump)
    return timesToJump

result = solution(10,85,30)
print(result)