# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(A):
    countPairs = 0
    
    for idx in range(0,len(A)):
        if A[idx] == 1:
            continue
        else:
            slice = A[idx+1:len(A)+1]
            count = sum(slice)
            countPairs = countPairs + count
    
    if countPairs > 1000000000:
        return -1
    else:
        return countPairs

A = [0, 1, 0, 1, 1]
result = solution(A)
print(result)
