# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(A):
    # write your code in Python 3.6
    # 1. search all double slices, then set into dslices
    dslices = list()
    
    for xIdx in range(0,len(A)):
        # has at list 3 positions
        maxXIdxAllowed = len(A) - 3
        if xIdx > maxXIdxAllowed:
            break
        else:
            for yIdx in range(xIdx + 1,len(A)):
                maxYIdxAllowed = len(A) - 2
                if yIdx > maxYIdxAllowed:
                    break
                else:
                    for zIdx in range(yIdx + 1,len(A)):
                        maxZIdxAllowed = len(A) - 1
                        if zIdx > maxZIdxAllowed:
                            break
                        else:
                            tmp_slice = (xIdx,yIdx,zIdx)
                            dslices.append(tmp_slice)
    
    #print(dslices)
    # 2. find maxSum of double slices
    maxSum = -10000*10000
    for dslice in dslices:
        x = dslice[0]
        y = dslice[1]
        z = dslice[2]
        leftSlice = A[x+1:y]
        rightSlice = A[y+1:z]
        tmp_sum = sum(leftSlice) + sum(rightSlice)
        if tmp_sum > maxSum:
            maxSum = tmp_sum
            
    return maxSum

A = [3, 2, 6, -1, 0]
result = solution(A)
print(result)
