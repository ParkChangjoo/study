# vector<int> solution(vector<int> A, K);
from numpy import random

def solution(A):
    A.sort()

    # temp var to store array item
    prevVal = 0
    for val in A:
        # remove values less than 0
        if val <= 0:
            pass
        else:
            # check if offset is greater than 1
            if val > prevVal + 1:
                return prevVal + 1
            else:
                prevVal = val
    # return maxVal + 1 when all the offset is 1
    return prevVal + 1

# Best
# 1. O(n2), No mistake
# 2. O(n), No mistake -> my target
# 3. Idea, No mistake

# test cases
# only minus
A = [-6,-5,-4,-3,-1]
result = solution(A)
print("A:",A)
print("Result:",result)

A = [-1]
result = solution(A)
print("A:",A)
print("Result:",result)

# mix minus and plus
A = [-2,-1,0,1,2]
result = solution(A)
print("A:",A)
print("Result:",result)

# only 0
A = [0]
result = solution(A)
print("A:",A)
print("Result:",result)

# only plus
A = [1]
result = solution(A)
print("A:",A)
print("Result:",result)

A = [1,3,4,5,6]
result = solution(A)
print("A:",A)
print("Result:",result)





#N = random.randint(1,100000)
#A = random.randint(-1000000,1000000, size=N)
#A = [1,3,4,5,6]
#Sorted = A.copy().sort()
#print(A)

# run function
#result = solution(A)
#print(result)