# -----------------------------------------
# Best
# -----------------------------------------
# 1. No mistake, O(n2)
# 2. No mistake, O(n) -> my target
# 3. No mistake, Idea

# -----------------------------------------
# To do
# -----------------------------------------
# 1. 테스트 케이스 만들기
# 2. 예외 케이스 만들기
# 3. 솔루션 만들기

# solution
def solution():
	pass

# Run
# 1. Test cases
from numpy import random

for tCount in range(5):
    A = [1,2,3,4,5]
    K = random.randint(0,5)
    print("A:",A)
    print("K:",K)
    result = solution(A,K)
    print("Result:",result)
    print("")
    
# 2. Exception cases
print("------------------")
for tCount in range(3):
    A = []
    K = random.randint(0,5)
    print("A:",A)
    print("K:",K)
    result = solution(A,K)
    print("Result:",result)
    print("")

# 3. Codility test
# (주석: ctrl+k, c 해제: ctrl+k, u)
# extreme_empty
# single
# double
# small1
# small2
# small_random_all_rotations
# medium_random
# maximal
#([],0)
#([],1)
#([1],2)
#([1,2],2)
#([1,2,3],2)
#([1,2,3],4)
#([1,2,3],3)
#([1,2,3,4,5,6,7],100)
#([1,2,3,4,5,6,7],1000)
