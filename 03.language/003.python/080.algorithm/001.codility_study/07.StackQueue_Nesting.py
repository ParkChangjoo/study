# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(S):
    # using stack
    # remove item from stack when item is ) and item in stack is (
    # len(stack) == 0 -> properly nested
    stack = list()
    for val in S:
        if len(stack) > 0:
            valInStack = stack[-1]
            if valInStack == "(" and val == ")":
                del stack[-1]
            else:
                stack.append(val)
        else:
            stack.append(val)
            
    if len(stack) == 0:
        return 1
    else:
        return 0

A = "((()()))"
result = solution(A)
print(result)