def solution(A):
    # write your code in Python 3.6
    # when A is empty
    if len(A) == 0:
        return 1

    # sort A
    A.sort()

    # check if offset between previous and now is greater than 1
    # if it is greater than 1, return previous + 1
    # if all the offset is 1, then return max item value + 1
    previous = 0
    for now in A:
        offset = now - previous
        # to next loop
        if offset > 1:
            return previous + 1
        else:
            previous = now
            
    return A[-1]+1

A = []
result = solution(A)
print(result)