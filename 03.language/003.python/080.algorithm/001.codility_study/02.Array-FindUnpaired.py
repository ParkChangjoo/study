# -----------------------------------------
# Best
# -----------------------------------------
# 1. No mistake, O(n2)
# 2. No mistake, O(n) -> my target
# 3. No mistake, Idea

# -----------------------------------------
# To do
# -----------------------------------------
# 1. 테스트 케이스 만들기
# 2. 예외 케이스 만들기
# 3. 솔루션 만들기

# solution
def solution(A):
    A.sort()

    
    pairs = [] # list to store pairs
    tempList = [] # temp list to create pair

    for x in A:
        # create pair
        if len(tempList) < 2:
            tempList.append(x)
        else:
            pass

        # move pair to pair list
        if len(tempList) == 2:
            pairs.append(tempList.copy())
            tempList.clear()

    # check pairs if values equal, if not return front value
    for pair in pairs:
        if pair[0] != pair[1]:
            return x[0]

    # when maximum number is unpaired number, return maximum number
    return tempList[0]

A = [1,2,1]
result = solution(A)
print("Result:",result)
print("")
