def solution(A):
    A.sort()
    
    result = 1
    
    prevValue = 0
    for value in A:
        offset = value - prevValue
        if offset == 0:
            result = 0
            break;
        elif offset > 1:
            result = 0
            break;
        else:
            prevValue = value
            
    return result

A = [1,2,3]
result = solution(A)
print(result)