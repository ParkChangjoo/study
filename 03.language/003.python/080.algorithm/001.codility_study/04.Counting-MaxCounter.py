# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(N, A):
    # write your code in Python 3.6
    Counter = N
    Result = list()
    for x in range(0, N):
        Result.insert(x,0)
    
    for action in A:
        if action >= 1 and action <= N:
            Result[action - 1] = Result[action - 1] + 1
        elif action == N + 1:
            # find max
            maxValue = max(Result)
            # set with max value
            for y in range(0,len(Result)):
                Result[y] = maxValue
        else:
            pass
        
    return Result

N = 5
A = [3, 4, 4, 6, 1, 4, 4]
result = solution(N, A)
print(result)
