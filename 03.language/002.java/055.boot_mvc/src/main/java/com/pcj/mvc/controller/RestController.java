package com.pcj.mvc.controller;

import com.pcj.mvc.model.RestModel;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RestController {

    // Rest Get
    // 1) Basic
    @GetMapping("/rest1")
    @ResponseBody
    public String rest1()
    {
        return "rest";
    }

    // 2) Query string
    @GetMapping("/rest2")
    @ResponseBody
    public String rest2(String name)
    {
        String toReturn = "name: " + name;
        return toReturn;
    }

    // 3) Path variable
    @GetMapping("/rest3/{name}")
    @ResponseBody
    public String rest3(@PathVariable String name)
    {
        String toReturn = "name: " + name;
        return toReturn;
    }

    // 4) Response model
    @GetMapping(value = "/rest4/{name}")
    @ResponseBody
    public RestModel rest4(@PathVariable() String name) {
        RestModel model = new RestModel();
        model.setName(name);

        return model;
    }

    // 5) Response type
    @GetMapping(value = "/rest5/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RestModel rest5(@PathVariable String name) {
        RestModel model = new RestModel();
        model.setName(name);

        return model;
    }
}