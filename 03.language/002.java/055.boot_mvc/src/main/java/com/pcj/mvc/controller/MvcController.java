package com.pcj.mvc.controller;

import com.pcj.mvc.model.MvcModel;
import com.pcj.mvc.model.MvcModel_Validation;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class MvcController {

    // GET
    // 1) header filter (by default header value)
    // - executed only when application/json exists.
    // curl -H "Content-Type: application/json" "http://localhost:5000/get1"
    @GetMapping(value = "/get1", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String get1() {
        return "get";
    }

    // 2) header filter (by custom header value)
    // curl -H "custom_header: abc" "http://localhost:5000/get2"
    @GetMapping(value = "/get2", headers = {"custom_header=abc"})
    public String get2() {
        return "get";
    }

    // 3) query string
    @GetMapping("/get3")
    public String get3(String name) {
        System.out.println("name: " + name);
        return "get";
    }

    // 4) query string (required)
    @GetMapping("/get4")
    public String get4(@RequestParam(name="name", required=true)String name) {
        System.out.println("name: " + name);
        return "get";
    }

    // 5) query string (default)
    @GetMapping("/get5")
    public String get5(@RequestParam(name="name", defaultValue="King")String name) {
        System.out.println("name: " + name);
        return "get";
    }

    // 6) query string + return model
    @GetMapping("/get6")
    public String get6(String name, Model model) {
        model.addAttribute("name", name);
        return "get_model";
    }

    // POST
    // 1) redirect
    // curl -i -X POST "http://localhost:5000/post1"
    @PostMapping("/post1")
    public String post1() {
        return "redirect:get1";
    }

    // 2) no input model
    // curl -i -X POST -d "name=wattagatta" "http://localhost:5000/post2"
    @PostMapping("/post2")
    public String post2(String name){
        System.out.println("name: " + name);
        return "redirect:get1";
    }

    // 3) input model
    // curl -i -X POST -d "name=wattagatta&age=50" "http://localhost:5000/post3"
    @PostMapping("/post3")
    public String post3(MvcModel model){
        System.out.println("name: " + model.getName());
        System.out.println("age: " + model.getAge());
        return "redirect:get1";
    }

    // 4) input model validation
    // curl -i -X POST -d "name=abcdefg&age=5" "http://localhost:5000/post4"
    @PostMapping("/post4")
    public String post4(@Valid MvcModel_Validation model, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            System.out.println("Error!!");
        }
        else {
            System.out.println("name: " + model.getName());
            System.out.println("age: " + model.getAge());
        }
        return "redirect:get1";
    }
}