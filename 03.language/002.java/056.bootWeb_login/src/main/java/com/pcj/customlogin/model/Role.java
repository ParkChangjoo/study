package com.pcj.customlogin.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Role {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long idx;
    private String roleName;

    protected Role(){}

    public Long getIdx() {
        return idx;
    }

    public void setIdx(Long idx) {
        this.idx = idx;
    }

    public String getRoleName(){
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}