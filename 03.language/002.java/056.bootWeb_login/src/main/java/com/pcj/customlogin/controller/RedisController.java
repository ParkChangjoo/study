package com.pcj.customlogin.controller;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Random;

@Controller
public class RedisController {

    private final RedisTemplate redisTemplate;

    public RedisController(RedisTemplate redisTemplate)
    {
        this.redisTemplate = redisTemplate;
    }


    @GetMapping("/redis")
    public String redis( Model model) {

        redisTemplate.opsForValue().set("test", new Random().nextInt());
        int val = (Integer)redisTemplate.opsForValue().get("test");
        model.addAttribute("val", val);

        return "redis";
    }
}
