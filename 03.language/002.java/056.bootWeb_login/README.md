# Description
spring boot 2.3.4 mvc website (jar build, embedded tomcat)  
+ custom login page, logout page

# Features
1. exception handling
2. logging
3. secure (csrf, xss, password hashing)
4. mysql (jpa, hikaricp)
5. redis (lettuce)
6. authentication, authorization
7. custom login page
8. custom logout page

# Login password
1234