# Description
lombok study

# Setting
1. Add gradle dependency
> compileOnly 'org.projectlombok:lombok'  
> annotationProcessor 'org.projectlombok:lombok'
2. Install intellij plugin
> Settings/Plugins
3. Change intellij annotation setting
> Build,Execution,Deployment/Compiler/Annotation processors/Check 'Enable annotation processing' for default