package com.pcj.lombok;

import com.pcj.lombok.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@Slf4j
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public ApplicationRunner applicationRunner() {
		return args -> {
			log.info("I am logback log with lombok's @Slf4j annotation.");

			Person person = new Person();
			person.setName("John");
			person.setAge(20);
			log.info("name: {}, age: {}", person.getName(), person.getAge());
		};
	}
}