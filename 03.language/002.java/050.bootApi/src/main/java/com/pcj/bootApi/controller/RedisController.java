package com.pcj.bootApi.controller;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class RedisController {

    private final RedisTemplate redisTemplate;

    public RedisController(RedisTemplate redisTemplate)
    {
        this.redisTemplate = redisTemplate;
    }

    @RequestMapping("/redis")
    public int redis() {

        redisTemplate.opsForValue().set("test", new Random().nextInt());
        int val = (Integer)redisTemplate.opsForValue().get("test");

        return val;
    }
}