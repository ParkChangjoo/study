package com.pcj.bootApi.config;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> errorHandler() {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("code", -1000);
        body.put("message", "server error");

        return new ResponseEntity<>(body, HttpStatus.OK);
    }
}