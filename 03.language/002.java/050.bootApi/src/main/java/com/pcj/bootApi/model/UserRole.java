package com.pcj.bootApi.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class UserRole {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long idx;
    private Long userIdx;
    private Long roleIdx;

    protected UserRole(){}

    public Long getIdx() {
        return idx;
    }

    public void setIdx(Long idx) {
        this.idx = idx;
    }

    public Long getUserIdx() {
        return userIdx;
    }

    public void setUserIdx(Long userIdx) {
        this.userIdx = userIdx;
    }

    public Long getRoleIdx() {
        return roleIdx;
    }

    public void setRoleIdx(Long roleIdx) {
        this.roleIdx = roleIdx;
    }
}