package com.pcj.bootApi.controller;

import com.pcj.bootApi.model.Role;
import com.pcj.bootApi.model.RoleRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HomeController {

    private final RoleRepository roleRepository;

    public HomeController(RoleRepository roleRepository)
    {
        this.roleRepository = roleRepository;
    }

    @RequestMapping("/")
    public String home() {
        return "Hello";
    }

    @RequestMapping("/role")
    public List<Role> role()
    {
        return roleRepository.findAll();
    }

    @GetMapping("/errortest")
    public String errortest() throws Exception {
        throw new Exception("error test");
    }
}
