# Description
spring boot 2.3.4 mvc api

# Features
1. exception handling
2. logging
3. mysql (jpa, hikaricp)
4. redis (lettuce)