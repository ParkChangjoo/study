# Description
spring boot 2.3.4 mvc website (war build)

# Features
1. exception handling
2. logging
3. secure (csrf, xss, password hashing)
4. mysql (jpa, hikaricp)
5. redis (lettuce)
6. authentication, authorization

# Login password
1234

# War build
## To build as war
1. build.gradle plugin 'war'
2. providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
3. Extends SpringBootServletInitializer (Application.java)
4. Override configure (Application.java)

## Deploy war to tomcat
1. rename build/libs/xxx.war -> ROOT.war
2. service tomcat8 stop (terminal)
3. cp ROOT.war to /var/lib/tomcat8/webapps
4. service tomcat8 start

## What is war file
war is tomcat deploy file