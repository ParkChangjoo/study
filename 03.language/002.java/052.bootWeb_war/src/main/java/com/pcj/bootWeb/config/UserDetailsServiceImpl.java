package com.pcj.bootWeb.config;

import com.pcj.bootWeb.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // find user with username from db -> get username, password, roles from db -> create UserDetails -> return UserDetails
        // not found -> throw UsernameNotFoundException
        com.pcj.bootWeb.model.User user = userRepository.findByUserName(username);

        if (user == null)
            throw new UsernameNotFoundException("User does not exist");
        else
        {
            List<UserRole> userRoles = userRoleRepository.findByUserIdx(user.getIdx());
            List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();

            userRoles.forEach(x -> {
                Role role = roleRepository.findById(x.getRoleIdx()).get();
                GrantedAuthority authority = new SimpleGrantedAuthority(role.getRoleName());
                grantList.add(authority);
            });

            UserDetails userDetails = new User(user.getUserName(), user.getPassword(), grantList);
            return userDetails;
        }
    }
}
