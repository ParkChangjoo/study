package com.pcj.bootWeb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;// war setting 3
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;// war setting 4
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	// do startup task here
	@Bean
	public CommandLineRunner commandLineRunner() {

		return args -> {

			Logger logger = LoggerFactory.getLogger(Application.class);
			logger.trace("clr: trace");
			logger.debug("clr: debug");
			logger.info("clr: info");
			logger.warn("clr: warn");
			logger.error("clr: error");

		};
	}

	// do startup task here
	@Bean
	public ApplicationRunner applicationRunner() {
		return args -> {

			Logger logger = LoggerFactory.getLogger(Application.class);
			logger.trace("ar: trace");
			logger.debug("ar: debug");
			logger.info("ar: info");
			logger.warn("ar: warn");
			logger.error("ar: error");
		};
	}
}
