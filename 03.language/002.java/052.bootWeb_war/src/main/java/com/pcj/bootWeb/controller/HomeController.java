package com.pcj.bootWeb.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.server.ResponseStatusException;

@Controller
public class HomeController {

    @GetMapping("/")
    public String home() {
        return "home";
    }

    @GetMapping("/adminOnly")
    public String adminOnly() {
        return "adminOnly";
    }

    @GetMapping("/qaOnly")
    public String qaOnly() {
        return "qaOnly";
    }

    @GetMapping("/403")
    public String Status403() {
        throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Unauthorized");
    }
}
