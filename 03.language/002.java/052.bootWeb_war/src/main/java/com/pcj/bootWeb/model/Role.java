package com.pcj.bootWeb.model;

import javax.persistence.*;

@Entity
public class Role {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long idx;
    private String roleName;

    protected Role(){}

    public Long getIdx() {
        return idx;
    }

    public void setIdx(Long idx) {
        this.idx = idx;
    }

    public String getRoleName(){
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}