package com.pcj.redis;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	// StringRedisTemplate: String-focused extension of RedisTemplate.
	@Bean
	public ApplicationRunner applicationRunner(StringRedisTemplate strTemplate) {
		return args -> {
			
			String key;

			// 0. FlushAll
			strTemplate.getConnectionFactory().getConnection().flushAll();

			// 1. String
			key = "myString";
			strTemplate.opsForValue().set(key, "1");
			String myString = strTemplate.opsForValue().get(key);
			System.out.println("myString: " + myString);

			strTemplate.opsForValue().increment(key);
			myString = strTemplate.opsForValue().get(key);
			System.out.println("myString: " + myString);

			strTemplate.opsForValue().decrement(key);
			myString = strTemplate.opsForValue().get(key);
			System.out.println("myString: " + myString);

			// 2. Set
			key = "mySet";
			strTemplate.opsForSet().add(key, "a", "b", "c");
			Set mySet = strTemplate.opsForSet().members(key);
			mySet.forEach(x -> {
				System.out.println("mySet: " + x.toString());
			});

			// 3. Sorted set
			key = "mySortedSet";
			strTemplate.opsForZSet().add(key, "a", 1);
			strTemplate.opsForZSet().add(key, "b", 2);
			Set mySortedSet = strTemplate.opsForZSet().range(key, 0, -1);
			mySortedSet.forEach(x -> {
				System.out.println("mySortedSet: " + x.toString());
			});

			// 4. Hash
			key = "myHash";
			Map<String, String> hashData = new HashMap<>();
			hashData.put("Name", "John");
			hashData.put("Age", "20");
			strTemplate.opsForHash().putAll(key, hashData);
			Map<Object, Object> myHash = strTemplate.opsForHash().entries(key);
			myHash.keySet().forEach(k -> {
				System.out.println("myHash key: " + k);
				System.out.println("myHash: " + myHash.get(k));
			});

			// 5. List
			key = "myList";
			strTemplate.opsForList().leftPush(key, "2");
			strTemplate.opsForList().leftPush(key, "1");
			strTemplate.opsForList().rightPush(key, "3");
			List<String> myList = strTemplate.opsForList().range(key, 0, -1);
			myList.forEach(x -> {
				System.out.println("myList: " + x);
			});

			// 6. Expire
			key = "myExpire";
			strTemplate.opsForValue().set(key, "expire test");
			strTemplate.expire(key, Duration.ofMillis(1000));
			String myExpire = strTemplate.opsForValue().get(key);
			System.out.println("myExpire: " + myExpire);
			Thread.sleep(1000);
			myExpire = strTemplate.opsForValue().get(key);
			System.out.println("myExpire: " + myExpire);
		};
	}
}
