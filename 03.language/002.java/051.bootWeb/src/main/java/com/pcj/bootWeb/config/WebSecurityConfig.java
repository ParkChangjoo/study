package com.pcj.bootWeb.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    // 1. to do (database user login)
    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    // 2. to do (database user login)
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsServiceImpl).passwordEncoder(passwordEncoder());
    }

    // 3. to do (role setting)
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // headers, csrf security are enabled. (default)
        // header security check: check response header by curl -i
        // csrf security check: check form source code if _csrf input exists

        http.httpBasic();// default login
        http.formLogin();// default login
        http.authorizeRequests().antMatchers("/adminOnly").access("hasRole('ROLE_ADMIN')");// Admin only
        http.authorizeRequests().antMatchers("/qaOnly").access("hasAnyRole('ROLE_QA', 'ROLE_ADMIN')");// Admin, QA only
        http.authorizeRequests().anyRequest().authenticated();// White list
        http.exceptionHandling().accessDeniedPage("/403");// When role is not allowed to access a page
    }
}