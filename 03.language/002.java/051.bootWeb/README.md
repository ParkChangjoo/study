# Description
spring boot 2.3.4 mvc website (jar build, embedded tomcat)

# Features
1. exception handling
2. logging
3. secure (csrf, xss, password hashing)
4. mysql (jpa, hikaricp)
5. redis (lettuce)
6. authentication, authorization

# Login password
1234