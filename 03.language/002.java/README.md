# Java study repository
## Description
This repository is to store Java program snippet I make.
## Folders
1. bootApi: spring boot 2.3.4 mvc api
2. bootWeb: spring boot 2.3.4 mvc website (jar build, embedded tomcat)
3. bootWeb (war): spring boot 2.3.4 mvc website (war build)
4. redis: redis study
5. lombok: lombok study