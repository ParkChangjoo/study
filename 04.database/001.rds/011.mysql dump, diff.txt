﻿1. dump
mysqldump -h endpoint -u userid --column-statistics=0 --routines=true --triggers=true -p --databases dbName > c:\download\dump.sql

2. diff
mysqldiff --server1=db1_userid:db1_password@db1_endpoint --server2=db2_userid:db2_password@db2_endpoint db1_dbName:db2_dbName --force --difftype=sql > c:\download\diff.sql