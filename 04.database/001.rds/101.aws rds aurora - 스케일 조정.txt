﻿1. 베이스라인 작성
2. 베이스라인의 각 항목별 수치 생성
3. 후보군 선택
4. 후보별 예상 적용 결과 생성
5. 후보중 선택
-----------------------------------
1. 베이스라인 작성 - Aws aurora mysql (innodb)
1) cpu		: CPUUtilization <= 50% 유지
2) memory	: BufferCacheHitRatio >= 99% 유지
3) memory	: 인스턴스 Memory size * 0.75 (innodb_buffer_pool_size: 75%) * 0.5 (50%) >= Innodb_buffer_pool_hit_rate 가 100% 이 되는 시점의 innodb_buffer_pool_usage 크기
(최소 hit_ratio가 100%이 되는 시점의 메모리 크기는 필요. 여유를 위해, 그 크기의 메모리 사이즈는 그 2배로 잡는다.)
4) 네트워크	: NetworkThroughput <= 인스턴스의 네트워크 성능 * 50%
5) connection	: DatabaseConnections <= 인스턴스의 max 커넥션 * 50%

2. 베이스라인의 각 항목별 수치 생성
- 최소 2주, 이상적 1개월의 데이터 필요
- 위 기간의 Percentile 50, Percentile 99, Percentile 1 확인 (옵션: 평균, Max, Min 확인)
- (옵션: 언제 피크치는지 패턴 확인)

1) CPUUtilization (CloudWatch)
2) BufferCacheHitRatio (CloudWatch)
3) Innodb_buffer_pool_hit_rate 100% 되는 시점의 innodb_buffer_pool_usage (Performance insight or db global status)
4) NetworkThroughput (CloudWatch)
5) DatabaseConnections (CloudWatch)

3. 후보군 선택
4. 호보별 예상 적용 결과 생성
5. 후보중 선택
-> 2~5번까지의 내용 파일로 작성